<?php
class Smartphone extends Model {
    public static $_table = 'smartphone';

    public function image_b64(){
        return sprintf("data:image/jpeg;base64,%s", base64_encode($this->Image));
    }

}